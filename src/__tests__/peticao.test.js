const supertest = require("supertest");
const app = require("../index");
const crypto = require('crypto');

const api = "/api/peticao";
const api_login = '/api/login';
const api_logout = '/api/logout';
const api_usuario = '/api/usuario';

const usuario_senha_body = { "username": "caio", "senha": "123" }
const peticao_valida = { "titulo": "Petição criada pelo usuário Caio2", "descricao": "Descrição teste", "meta_assinatura": 3, "travar_meta_assinatura": true}
const failed_token = { "auth": false, "message": "Failed to authenticate token." }
const mensagem_campos_incorretos = "Campos não foram preenchidos corretamente.";
const peticao_invalida = "aaaaaaaaaaaaaaaaaaaaaaaa";
const mensagem_login_invalido = "Login inválido!"

describe("peticoes", () => {
    
    describe('post peticao', () => {

        describe('post a valid peticao', () => {
            it('Should return a 200', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send(peticao_valida)
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(200);
                    });
            });
            it('Should return a 500', async () => {
                await supertest(app)
                    .post(api)
                    .send(peticao_valida)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .set('Authorization', 'invalid-token')
                    .expect(500)
                    .then(response => {
                        expect(response.body).toEqual(
                            expect.objectContaining(failed_token)
                        );
                    });
            });
        });

        describe('post an invalid peticao', () => {
            it('Should return a 404', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send({ "t": "Petição criada pelo usuário Caio2", "desc": "Descrição teste" })
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(404)
                            .then(response => {
                                expect(response.body.msg).toContain(mensagem_campos_incorretos);
                            })
                    });
            });
        });
    });

    describe('put peticao', () => {
        describe('put a valid peticao', () => {
            it('Should return a 200', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token;
                        await supertest(app)
                            .post(api)
                            .send(peticao_valida)
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(200);
                    });

            });
            it('Should return a 500', async () => {
                await supertest(app)
                    .post(api)
                    .send(peticao_valida)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .set('Authorization', 'invalid-token')
                    .expect(500)
                    .then(response => {
                        expect(response.body).toEqual(
                            expect.objectContaining(failed_token)
                        );
                    });
            });
        });

        describe('put an invalid peticao', () => {
            it('Should return a 404', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send({ "t": "Petição criada pelo usuário Caio2", "desc": "Descrição teste" })
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(404)
                            .then(response => {
                                expect(response.body.msg).toContain(mensagem_campos_incorretos);
                            });
                    });

            });
        });
    });

    describe("get peticoes", () => {
        it("should return a 200", async () => {
            await supertest(app)
                .get("/api/peticoes")
                .expect(200)
                .then(async response => {
                    await supertest(app)
                        .get(`${api}/${response.body[0]._id}`)
                        .expect(200);
                });
        });
    });

    describe('sign peticao', () => {
        describe('sign a valid peticao', () => {
            it('Should return a 200', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send(peticao_valida)
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(200)
                            .then(async response => {
                                await supertest(app)
                                    .put(`${api}/${response.body.retorno.insertedId}/assinar`)
                                    .set('Authorization', token)
                                    .expect(200);
                            });
                    });

            });
            it('Should return a 500', async () => {
                await supertest(app)
                    .get("/api/peticoes")
                    .expect(200)
                    .then(async response => {
                        await supertest(app)
                            .put(`${api}/${response.body[0]._id}/assinar`)
                            .set('Authorization', 'invalid-token')
                            .expect(500)
                            .then(response => {
                                expect(response.body).toEqual(
                                    expect.objectContaining(failed_token)
                                );
                            });
                    });
            });
        });
        describe('sign an invalid peticao', () => {
            it('Should return a 404', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .put(`${api}/${peticao_invalida}/assinar`)
                            .set('Authorization', token)
                            .expect(404);
                    });
            });
        });
    });

    describe('delete peticao', () => {
        describe('delete a valid peticao', () => {
            it('Should return a 200', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send(peticao_valida)
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(200)
                            .then(async response => {
                                await supertest(app)
                                    .delete(`${api}/${response.body.retorno.insertedId}`)
                                    .set('Authorization', token)
                                    .expect(200);
                            });
                    });
            });
            it('Should return a 401', async () => {
                await supertest(app)
                    .post(api_login)
                    .send(usuario_senha_body)
                    .set('Content-Type', 'application/json')
                    .set('Accept', 'application/json')
                    .expect(200)
                    .then(async response => {
                        const token = response.body.token
                        await supertest(app)
                            .post(api)
                            .send(peticao_valida)
                            .set('Content-Type', 'application/json')
                            .set('Accept', 'application/json')
                            .set('Authorization', token)
                            .expect(200)
                            .then(async response => {
                                await supertest(app)
                                    .delete(`${api}/${response.body.retorno.insertedId}`)
                                    .set('Authorization', 'invalid-token')
                                    .expect(500)
                                    .then(response => {
                                        expect(response.body).toEqual(
                                            expect.objectContaining(failed_token)
                                        );
                                    });
                            });
                    });
            });
        });
    });
});


