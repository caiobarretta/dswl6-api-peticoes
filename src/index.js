const app = require('./config/server.js');
const routes = require("./app/routes/routes");

//O sistema deve possibilitar inclusão, alteração, deleção e consulta de petições
routes.getPeticoes(app);
routes.getPeticao(app);
routes.addPeticao(app);
routes.alterPeticao(app);
routes.delPeticao(app);
routes.signPeticao(app);
routes.login(app);
routes.logout(app);
routes.addUsuario(app);
routes.alterUsuario(app);

module.exports = app;