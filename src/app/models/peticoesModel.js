const client = require('../../config/dbConnection');
const { ObjectId } = require("bson");


module.exports = class PeticoesModel {

    static async getPeticoes() {
        console.log(`[getallpeticoes]`);
        const cursor = await client.db("dsw").collection("peticoes").find();
        const peticoes = await cursor.toArray();
        return peticoes;
    }
    static async getPeticao(id) {
        console.log(`[getpeticaobyid]_id:${id}`);
        const cursor = await client.db("dsw").collection("peticoes").find({ "_id": ObjectId(`${id}`) });
        const peticao = await cursor.toArray();
        return peticao;
    }

    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Petição pode ter uma meta de assinatura
    //Usuário que criou ou o admninistrador pode escolher travar a petição na meta ou não.
    static async addPeticao(data) {
        console.log(`[Peticao Model - Add Peticao] ${data}`);
        try {
            //As petições devem ter no mínimo titulo, descricao, usuario que a criou, data de criação
            const newPeticao = {
                titulo: data.titulo, descricao: data.descricao, usuario_criacao: data.usuario,
                meta_assinatura: data.meta_assinatura, travar_meta_assinatura: data.travar_meta_assinatura,
                data_de_criacao: new Date()
            }

            console.log(`[Peticao Model - Add Peticao] ${newPeticao}`);
            const addedPeticao = await client.db("dsw").collection("peticoes").insertOne(newPeticao);
            console.log(`New peticao inserted with the following id ${addedPeticao.insertedId}`);
            return addedPeticao;
        } catch (error) {
            console.log(`[peticaoService] Error: ${error}`);
            return error;
        }
    }

    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Petição pode ter uma meta de assinatura
    //Usuário que criou ou o admninistrador pode escolher travar a petição na meta ou não.
    static async alterPeticao(id, data) {
        console.log(`[Peticao Model - Alter Peticao] ${data}`);
        try {
            //As petições devem ter no mínimo titulo, descricao, usuario que a criou, data de criação
            const peticao = {
                titulo: data.titulo, descricao: data.descricao, usuario_alteracao: data.usuario,
                meta_assinatura: data.meta_assinatura, travar_meta_assinatura: data.travar_meta_assinatura,
                data_alteracao: new Date()
            }
            const updatePeticao = await client.db("dsw").collection("peticoes").updateOne({ _id: ObjectId(`${id}`) },
                [
                    { $set: peticao }
                ]);
            console.log(`The peticao updated with the following id ${id}`);
            return updatePeticao;
        } catch (error) {
            console.log(`[peticaoService] Error: ${error}`);
            return error;
        }
    }
    static async delPeticao(id) {
        console.log(`[delpeticaobyid]_id:${id}`);
        try {
            await client.db("dsw").collection("peticoes").deleteOne({ "_id": ObjectId(id) });
        } catch (error) {
            console.log(`[peticaoService] Error: ${error}`);
            return error;
        }
    }

    static async signPeticao(id, data, assinaturas_peticao) {
        console.log(`[Peticao Model - Sign Peticao] data ${data}`);
        assinaturas_peticao.push(data.usuario);
        console.log(`[Peticao Model - Sign Peticao] assinaturas  ${assinaturas_peticao}`);
        try {
            //Somente usuário autenticados podem assinar petições
            const peticao = {
                assinaturas: assinaturas_peticao
            }
            const updatePeticao = await client.db("dsw").collection("peticoes").updateOne({ _id: ObjectId(`${id}`) },
                [
                    { $set: peticao }
                ]);
            console.log(`The peticao updated with the following id ${id}`);
            return updatePeticao;
        } catch (error) {
            console.log(`[peticaoService] Error: ${error}`);
            return error;
        }
    }

    static isValid(peticao) {
        if (!peticao || (Array.isArray(peticao) && !peticao.length)) {
            console.log('Não existe nenhuma petição cadastrada.');
            return false;
        }
        return true;
    }

    static isUserOwnerPeticao(peticao, usuario) {
        return peticao[0].usuario_criacao == usuario;
    }
}
