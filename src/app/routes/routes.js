const Peticoes = require("../controllers/peticoes");
const Users = require("../controllers/usuarios");

//O sistema deve possibilitar inclusão, alteração, deleção e consulta de petições
module.exports = {
    //O sistema deve possibilitar consulta de petições
    //Qualquer usuário pode ver as petições
    getPeticoes: (app) => {
        app.get('/api/peticoes', Peticoes.getPeticoes);
    },

    getPeticao: (app) => {
        app.get('/api/peticao/:id', Peticoes.getPeticao);
    },

    //O sistema deve possibilitar inclusão
    //Somente usuário autenticados podem criar petições
    addPeticao: (app) => {
        app.post('/api/peticao', Users.verifyJWT, Peticoes.addPeticao);
    },

    //O sistema deve possibilitar alteração
    //Somente quem pode alterar petições é o usuário que criou
    alterPeticao: (app) => {
        app.put('/api/peticao/:id', Users.verifyJWT, Peticoes.alterPeticao);
    },

    //O sistema deve possibilitar deleção
    //Somente quem pode excluir petições é o usuário que criou
    delPeticao: (app) => {
        app.delete('/api/peticao/:id', Users.verifyJWT,  Peticoes.delPeticao);
    },

    //Somente usuário autenticados podem assinar petições
    signPeticao: (app) => {
        app.put('/api/peticao/:id/assinar', Users.verifyJWT, Peticoes.signPeticao);
    },

    login: (app) =>{
        app.post('/api/login', Users.getUsuarioByUsuarioSenha);
    },

    logout: (app) =>{
        app.post('/api/logout', Users.logout);
    },

    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Adicionar usuários
    addUsuario: (app) =>{
        app.post('/api/usuario', Users.addUsuario);
    },

    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Alterar usuários
    alterUsuario: (app) => {
        app.put('/api/usuario/:id', Users.verifyJWT, Users.alterUsuario);
    },
}
