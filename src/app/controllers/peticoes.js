const PeticoesModel = require("../models/peticoesModel");
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi)

//As petições devem ter no mínimo titulo, descricao, usuario que a criou, data de criação
//titulo: data.titulo, descricao: data.descricao, usuario_criacao: data.usuario, data_de_criacao: new Date() }


//Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
//Petição pode ter uma meta de assinatura
//Usuário que criou ou o admninistrador pode escolher travar a petição na meta ou não.
const schema = Joi.object().keys({
    titulo: Joi.string().required().min(1).max(50),
    descricao: Joi.string().required().min(1).max(500),
    usuario: Joi.objectId(),
    meta_assinatura: Joi.number().required().min(0),
    travar_meta_assinatura: Joi.bool().required(),
    admin: Joi.bool().required()
});

module.exports = class PeticoesController {
    //Qualquer usuário pode ver as petições
    static async getPeticoes(req, res, next) {
        try {
            const peticoes = await PeticoesModel.getPeticoes();
            if (!peticoes) {
                return res.status(404).json(`Não existe nenhuma petição cadastrada.`);
            }
            return res.status(200).json(peticoes);
        } catch (error) {
            console.log(`[Movies Controller Error] ${error}`);
            res.status(500).json({ error: error })
        }
    }

    //Qualquer usuário pode ver as petições
    static async getPeticao(req, res, next) {
        try {
            const id = req.params.id;
            const peticao = await PeticoesModel.getPeticao(id);
            if (!peticao) {
                return res.status(404).json(`Não existe nenhuma petição cadastrada.`);
            }
            return res.status(200).json(peticao);
        } catch (error) {
            console.log(`[Movies Controller Error] ${error}`);
            return res.status(500).json({ error: error })
        }
    }
    //O sistema deve possibilitar inclusão
    //Somente usuário autenticados podem criar petições
    static async addPeticao(req, res, next) {
        console.log('PeticoesController - addPeticao', req.body);
        const { error, value } = schema.validate(req.body);
        if (error) {
            const result = {
                msg: 'Petição não incluída. Campos não foram preenchidos corretamente.',
                error: error.details
            }
            return res.status(404).json(result);
        }
        try {
            const added = await PeticoesModel.addPeticao(req.body);
            return res.status(200).json({ msg: 'added', 'retorno': added });
        } catch (error) {
            return res.status(500).json({ error: error });
        }
    }
    //O sistema deve possibilitar alteração
    //Somente quem pode alterar petições é o usuário que criou
    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Usuários administradores podem alterar qualquer petição
    static async alterPeticao(req, res, next) {
        console.log('PeticoesController - alterPeticao');
        const { error, value } = schema.validate(req.body);
        if (error) {
            const result = {
                msg: 'Petição não alterada. Campos não foram preenchidos corretamente.',
                error: error.details
            }
            return res.status(404).json(result);
        }

        try {
            //Somente quem pode alterar petições é o usuário que criou
            //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
            //Usuários administradores podem alterar qualquer petição
            const peticao = await PeticoesModel.getPeticao(req.params.id);
            var isValid = PeticoesModel.isValid(peticao);
            if (!isValid)
                return res.status(404).json(`Não existe nenhuma petição cadastrada.`);

            if (!PeticoesModel.isUserOwnerPeticao(peticao, req.body.usuario) && !req.body.admin)
                return res.status(404).json({ msg: `Somente quem pode alterar petições é o usuário que criou ou administradores` });

            const alter = await PeticoesModel.alterPeticao(req.params.id, req.body);
            console.log('alter:', alter);
            return res.status(200).json({ msg: "Alterado", "retorno": alter });
        } catch (error) {
            return res.status(500).json({ error: error });
        }
    }

    //O sistema deve possibilitar deleção
    //Somente quem pode excluir petições é o usuário que criou
    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Usuários administradores podem deletar qualquer petição
    static async delPeticao(req, res, next) {
        try {
            const peticao = await PeticoesModel.getPeticao(req.params.id);
            var isValid = PeticoesModel.isValid(peticao);
            if (!isValid)
                return res.status(404).json(`Não existe nenhuma petição cadastrada.`);

            if (!PeticoesModel.isUserOwnerPeticao(peticao, req.body.usuario) && !req.body.admin)
                return res.status(404).json({ msg: `Somente quem pode alterar petições é o usuário que criou ou administradores` });

            const deleted = await PeticoesModel.delPeticao(req.params.id);
            console.log('deleted:', deleted);
            return res.status(200).json({ msg: "Deletado", 'retorno': deleted });
        } catch (error) {
            return res.status(500).json({ error: error })

        }
    }

    //Somente usuário autenticados podem assinar petições
    //Pense em uma funcionalidade que o sistema possa ter a mais do que foi pedido e implemente.
    //Petição pode ter uma meta de assinatura
    //Usuário que criou ou o admninistrador pode escolher travar a petição na meta ou não.
    static async signPeticao(req, res, next) {
        console.log('signPeticao');
        try {
            const peticao = await PeticoesModel.getPeticao(req.params.id);
            var isValid = PeticoesModel.isValid(peticao);
            if (!isValid)
                return res.status(404).json(`Não existe nenhuma petição cadastrada.`);

            console.log('[signPeticao]: peticao', peticao[0]);
            var assinaturas = []
            if (peticao[0].assinaturas) {
                if (peticao[0].travar_meta_assinatura && peticao[0].assinaturas.length >= peticao[0].meta_assinatura){
                    console.log('[signPeticao]: a já petição bateu a meta de assinaturas');
                    return res.status(404).json(`a já petição bateu a meta de assinaturas.`);
                }

                if (peticao[0].assinaturas.includes(req.body.usuario)) {
                    console.log('[signPeticao]: usuário já assinou petição');
                    return res.status(404).json(`usuário já assinou petição.`);
                }
                assinaturas = peticao[0].assinaturas;
            }
            const signed = await PeticoesModel.signPeticao(req.params.id, req.body, assinaturas);
            console.log('signed:', signed);
            return res.status(200).json({ msg: "signed", 'retorno': signed });
        } catch (error) {
            return res.status(500).json({ error: error })
        }
    }
}