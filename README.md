# DSWL6 API Petições

Collection postman anexa [aqui](./dswl6-api-peticoes.postman_collection.json)

## Funcionalidade a mais que foram implementadas:
* Adicionar usuários.
* Alterar usuários.
* Usuários administradores podem alterar qualquer petição.
* Petição pode ter uma meta de assinatura (Usuário que criou ou o admninistrador pode escolher travar a petição na meta ou não).

## Rotas:

### Não necessário autenticação

<img src="https://img.shields.io/badge/METHOD-GET-green">

Obter todas as petições: `"/api/peticoes"`

Obter petição a partir de um id: `"/api/peticao/:id"`

---

### Necessário autenticação de usuário

<img src="https://img.shields.io/badge/METHOD-POST-orange">

Adicionar uma petição: 
  * endpoint: `"/api/peticao/:id"`
  * body: `{ "titulo": "XXXX","descricao": "XXXX", "meta_assinatura": 0, "travar_meta_assinatura": true }`
  * header bearer token: token passado

Adicionar usuário:  
  * endpoint: `"/api/usuario"`
  * body: `{ "username": "XXXX", "senha":"XXXX", "admin": false}`
  * retorno positivo: será retornado um token o qual deve ser utilizado para funcionalidades de adição ou edição

Logar usuário: 
  * endpoint: `"/api/login"`
  * body: `{ "username": "XXXX", "senha":"XXXX"}`

Logout usuário: `"/api/logout"`

<img src="https://img.shields.io/badge/METHOD-PUT-blue">

Alterar usuário: 
  * endpoint: `"/api/usuario/:id"`
  * body: `{ "username": "XXXX", "senha":"XXXX"}`
  * header bearer token: token passado

Alterar uma petição:
  * endpoint: `"/api/peticao/:id"`
  * body: `{ "titulo": "XXXX","descricao": "XXXX", "meta_assinatura": 0, "travar_meta_assinatura": true}`
  * header bearer token: token passado

Assinar uma petição:
  * endpoint: `"/api/peticao/:id/assinar"`
  * header bearer token: token passado


<img src="https://img.shields.io/badge/METHOD-DELETE-red">

Deletar uma petição:
  * endpoint: `"/api/peticao/:id"`
  * header bearer token: token passado
